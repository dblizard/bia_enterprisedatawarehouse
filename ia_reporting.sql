---------------------------------------------------------------------------

DROP TABLE dim_admit_type;

CREATE TABLE dim_admit_type
(
  admit_type_pk     NUMBER(11,0) NOT NULL,
  admit_type_code   VARCHAR2(1 CHAR) NOT NULL,
  admit_type_desc   VARCHAR2(30 CHAR) NOT NULL,
  create_timestamp  TIMESTAMP  WITH LOCAL TIME ZONE NOT NULL,
  create_module     VARCHAR2(32) NOT NULL,
  upd_timestamp     TIMESTAMP WITH LOCAL TIME ZONE,
  upd_module        VARCHAR2(32),
  CONSTRAINT pk_dim_admit_type PRIMARY KEY ( admit_type_pk ) ENABLE
);

DROP SEQUENCE dim_admit_type_seq; 
CREATE SEQUENCE dim_admit_type_seq;

---------------------------------------------------------------------------

DROP TABLE dim_age_band;

CREATE TABLE dim_age_band
(
  age_band_pk         NUMBER(11,0) NOT NULL,
  age_years           NUMBER,
  student_age_band    VARCHAR2(30 CHAR),
  workforce_age_band  VARCHAR2(30 CHAR),
  create_timestamp    TIMESTAMP  WITH LOCAL TIME ZONE NOT NULL,
  create_module       VARCHAR2(32) NOT NULL,
  upd_timestamp       TIMESTAMP WITH LOCAL TIME ZONE,
  upd_module          VARCHAR2(32),
  CONSTRAINT pk_dim_age_band PRIMARY KEY ( age_band_pk) ENABLE
);

DROP SEQUENCE dim_age_band_seq; 
CREATE SEQUENCE dim_age_band_seq;

---------------------------------------------------------------------------

DROP TABLE dim_stud_class;

CREATE TABLE dim_stud_class
(
  class_pk          NUMBER(11,0) NOT NULL,
  class_code        VARCHAR2(2 CHAR) NOT NULL,
  class_desc        VARCHAR2(30 CHAR) NOT NULL,
  class_group_desc  VARCHAR2(30 CHAR) NOT NULL,
  create_timestamp  TIMESTAMP  WITH LOCAL TIME ZONE NOT NULL,
  create_module     VARCHAR2(32) NOT NULL,
  upd_timestamp     TIMESTAMP WITH LOCAL TIME ZONE,
  upd_module        VARCHAR2(32),
  CONSTRAINT pk_dim_class PRIMARY KEY ( class_pk ) ENABLE
);

DROP SEQUENCE dim_stud_class_seq; 
CREATE SEQUENCE dim_stud_class_seq;

---------------------------------------------------------------------------

DROP TABLE dim_ethnicity;

CREATE TABLE dim_ethnicity
(
  ethnicity_pk          NUMBER(11,0) NOT NULL,
  ethnicity_code        VARCHAR2(2 CHAR) NOT NULL,
  ethnicity_desc        VARCHAR2(30 CHAR) NOT NULL,
  ethnicity_group_desc  VARCHAR2(30 CHAR) NOT NULL,
  create_timestamp      TIMESTAMP  WITH LOCAL TIME ZONE NOT NULL,
  create_module         VARCHAR2(32) NOT NULL,
  upd_timestamp         TIMESTAMP WITH LOCAL TIME ZONE,
  upd_module            VARCHAR2(32),
  CONSTRAINT pk_dim_ethnicity PRIMARY KEY ( ethnicity_pk ) ENABLE
);

DROP SEQUENCE dim_ethnicity_seq; 
CREATE SEQUENCE dim_ethnicity_seq;

---------------------------------------------------------------------------

DROP TABLE dim_first_gen;

CREATE TABLE dim_first_gen
(
  first_gen_pk          NUMBER(11,0) NOT NULL,
  first_gen_code        VARCHAR2(1 CHAR) NOT NULL,
  first_gen_short_desc  VARCHAR2(10 CHAR) NOT NULL,
  create_timestamp      TIMESTAMP  WITH LOCAL TIME ZONE NOT NULL,
  create_module         VARCHAR2(32) NOT NULL,
  upd_timestamp         TIMESTAMP WITH LOCAL TIME ZONE,
  upd_module            VARCHAR2(32),
  CONSTRAINT pk_dim_first_gen PRIMARY KEY ( first_gen_pk ) ENABLE
);

DROP SEQUENCE dim_first_gen_seq; 
CREATE SEQUENCE dim_first_gen_seq;

---------------------------------------------------------------------------

DROP TABLE dim_gender;

CREATE TABLE dim_gender
(
  gender_pk         NUMBER(11,0) NOT NULL,
  gender_code       VARCHAR2(1 CHAR),
  gender_desc       VARCHAR2(20 CHAR),
  gender_desc_2     VARCHAR2(20 CHAR),
  create_timestamp  TIMESTAMP  WITH LOCAL TIME ZONE NOT NULL,
  create_module     VARCHAR2(32) NOT NULL,
  upd_timestamp     TIMESTAMP WITH LOCAL TIME ZONE,
  upd_module        VARCHAR2(32),
  CONSTRAINT pk_dim_gender PRIMARY KEY ( gender_pk) ENABLE
);

DROP SEQUENCE dim_gender_seq; 
CREATE SEQUENCE dim_gender_seq;

---------------------------------------------------------------------------

DROP TABLE dim_srvc_intl_dom;

CREATE TABLE dim_srvc_intl_dom
(
  intl_dom_pk         NUMBER(11,0) NOT NULL,
  intl_dom_code       VARCHAR2(10),
  intl_dom_desc       VARCHAR2(30 CHAR),
  intl_dom_category   VARCHAR2(30 CHAR),
  create_timestamp    TIMESTAMP  WITH LOCAL TIME ZONE NOT NULL,
  create_module       VARCHAR2(32) NOT NULL,
  upd_timestamp       TIMESTAMP WITH LOCAL TIME ZONE,
  upd_module          VARCHAR2(32),
  CONSTRAINT pk_dim_srvc_intl_dom PRIMARY KEY (intl_dom_pk) ENABLE
);

DROP SEQUENCE dim_srvc_intl_dom_seq; 
CREATE SEQUENCE dim_srvc_intl_dom_seq;

---------------------------------------------------------------------------

DROP TABLE dim_stud_level;

CREATE TABLE dim_stud_level
(
  level_pk          NUMBER(11,0) NOT NULL,
  level_code        VARCHAR2(2 CHAR) NOT NULL,
  level_desc        VARCHAR2(30 CHAR) NOT NULL,
  grad_level        VARCHAR2(15 CHAR) NOT NULL,
  create_timestamp  TIMESTAMP  WITH LOCAL TIME ZONE NOT NULL,
  create_module     VARCHAR2(32) NOT NULL,
  upd_timestamp     TIMESTAMP WITH LOCAL TIME ZONE,
  upd_module        VARCHAR2(32),
  CONSTRAINT pk_dim_level PRIMARY KEY ( level_pk ) ENABLE
);

DROP SEQUENCE dim_stud_level_seq; 
CREATE SEQUENCE dim_stud_level_seq;

---------------------------------------------------------------------------

DROP TABLE dim_stud_term;

CREATE TABLE dim_stud_term
(
  term_pk                   NUMBER(11,0) NOT NULL,
  banner_term_code          VARCHAR2(6) NOT NULL,
  term_desc                 VARCHAR2(30) NOT NULL,
  term_cal_yr_desc          VARCHAR2(35) NOT NULL,
  term_cal_yr_short_desc    VARCHAR2(30) NOT NULL,
  semester_quarter          VARCHAR2(15) NOT NULL,
  season                    VARCHAR2(15) NOT NULL,
  first_day_of_instruction  DATE NOT NULL,
  last_day_of_instruction   DATE NOT NULL,
  census_date               DATE NOT NULL,
  fiscal_year               VARCHAR2(7) NOT NULL,
  cal_yr_ending             VARCHAR2(4) NOT NULL, 
  acad_year                 VARCHAR2(7) NOT NULL,
  acad_year_ending          VARCHAR2(4) NOT NULL,
  acad_year_long            VARCHAR2(9) NOT NULL,
  aid_yr                    VARCHAR2(7) NOT NULL,
  aid_yr_ending             VARCHAR2(4) NOT NULL,
  aid_yr_long               VARCHAR2(9) NOT NULL,
  create_timestamp          TIMESTAMP  WITH LOCAL TIME ZONE NOT NULL,
  create_module             VARCHAR2(32) NOT NULL,
  upd_timestamp             TIMESTAMP WITH LOCAL TIME ZONE,
  upd_module                VARCHAR2(32),
  CONSTRAINT pk_dim_term PRIMARY KEY ( term_pk ) ENABLE
);

DROP SEQUENCE dim_stud_term_seq; 
CREATE SEQUENCE dim_stud_term_seq;

---------------------------------------------------------------------------

DROP TABLE fact_student_enrollment;

CREATE TABLE fact_student_enrollment
(
  epidm              VARCHAR2(20) NOT NULL,
  head_count         NUMBER(2,0) NOT NULL,
  unit_attempted     NUMBER(3,1) NOT NULL,
  term_fk            NUMBER(11,0) NOT NULL,
  admit_type_fk      NUMBER(11,0) NOT NULL,
  age_fk             NUMBER(11,0) NOT NULL,
  class_fk           NUMBER(11,0) NOT NULL,
  svc_intl_dom_fk    NUMBER(11,0) NOT NULL,
  primary_major_fk   NUMBER(11,0) NOT NULL,
  first_gen_fk       NUMBER(11,0) NOT NULL,
  gender_fk          NUMBER(11,0) NOT NULL,
  ethnicity_fk       NUMBER(11,0) NOT NULL,
  level_fk           NUMBER(11,0) NOT NULL
);

---------------------------------------------------------------------------

grant all on DIM_ADMIT_TYPE to ia_staging;
grant all on DIM_AGE_BAND to ia_staging;
grant all on DIM_STUD_CLASS to ia_staging;
grant all on DIM_ETHNICITY to ia_staging;
grant all on DIM_FIRST_GEN to ia_staging;
grant all on DIM_GENDER to ia_staging;
grant all on DIM_SRVC_INTL_DOM to ia_staging;
grant all on DIM_STUD_LEVEL to ia_staging;
grant all on DIM_STUD_TERM to ia_staging;
grant all on FACT_STUDENT_ENROLLMENT to ia_staging;

grant select on DIM_ADMIT_TYPE_SEQ to ia_staging;
grant select on DIM_AGE_BAND_SEQ to ia_staging;
grant select on DIM_STUD_CLASS_SEQ to ia_staging;
grant select on DIM_ETHNICITY_SEQ to ia_staging;
grant select on DIM_FIRST_GEN_SEQ to ia_staging;
grant select on DIM_GENDER_SEQ to ia_staging;
grant select on DIM_SRVC_INTL_DOM_SEQ to ia_staging;
grant select on DIM_STUD_LEVEL_SEQ to ia_staging;
grant select on DIM_STUD_TERM_SEQ to ia_staging;