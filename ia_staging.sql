SCHEMA IA_STAGING

------------------------------------------------------------------------------------------------------------------------

DROP TABLE source_admit_type;

CREATE TABLE source_admit_type
(
  admit_type_code  VARCHAR2(1 CHAR),
  admit_type_desc  VARCHAR2(30 CHAR)
);

DROP TABLE psa_admit_type; 

CREATE TABLE psa_admit_type
(
  admit_type_code VARCHAR2(1 CHAR), 
  admit_type_desc VARCHAR2(30 CHAR),
  dls_admit_type  VARCHAR2(1 CHAR),
  upd_timestamp   TIMESTAMP,
  action          VARCHAR2(10 CHAR)
);

CREATE OR REPLACE FORCE VIEW trnsfrm_admit_type_v
( 
  admit_type_code, 
  admit_type_desc,
  admit_type_rank
)
AS
SELECT admit_type_code, 
       admit_type_desc,
       RANK() OVER (PARTITION BY admit_type_code ORDER BY admit_type_code, upd_timestamp desc) AS admit_type_rank
FROM   psa_admit_type
WHERE  dls_admit_type = 'Y'
AND    admit_type_desc IS NOT NULL;

------------------------------------------------------------------------------------------------------------------------

DROP TABLE source_age_band; 

CREATE TABLE source_age_band
(
  age_years           NUMBER,
  student_age_band    VARCHAR2(30 CHAR),
  workforce_age_band  VARCHAR2(30 CHAR)
);

DROP TABLE psa_age_band; 

CREATE TABLE psa_age_band
(
  age_years           NUMBER,
  student_age_band    VARCHAR2(30 CHAR),
  workforce_age_band  VARCHAR2(30 CHAR),
  dls_age_band        VARCHAR2(1 CHAR),
  upd_timestamp       TIMESTAMP,
  action              VARCHAR2(10)
);

CREATE OR REPLACE FORCE VIEW trnsfrm_age_band_v
( 
  age_years,
  student_age_band,
  workforce_age_band,
  age_band_rank
)
AS
SELECT age_years,
       student_age_band,
       workforce_age_band,
       RANK() OVER (PARTITION BY age_years ORDER BY age_years, upd_timestamp desc) AS age_band_rank
FROM   psa_age_band
WHERE  dls_age_band = 'Y'
AND    student_age_band IS NOT NULL
AND    workforce_age_band IS NOT NULL;

------------------------------------------------------------------------------------------------------------------------

DROP TABLE source_stvclas; 

CREATE TABLE source_stvclas
  AS SELECT * FROM stvclas@banner WHERE 1=2;

DROP TABLE psa_stvclas; 

CREATE TABLE psa_stvclas
(
  stvclas_code    VARCHAR2(2 CHAR),
  stvclas_desc    VARCHAR2(30 CHAR),
  dls_class       VARCHAR2(1 CHAR),
  upd_timestamp   TIMESTAMP,
  action          VARCHAR2(10 CHAR)
);

CREATE OR REPLACE FORCE VIEW trnsfrm_stud_class_v
( 
  class_code,
  class_desc,
  class_group_desc,
  class_rank
)
AS
SELECT stvclas_code,
       stvclas_desc,
       CASE WHEN stvclas_desc LIKE 'Doctorate%'
              THEN 'Doctorate'
            WHEN stvclas_desc LIKE 'Law%'
              THEN 'Law School'
            WHEN stvclas_desc IN ('Freshman', 'Sophmore', 'Junior', 'Senior')
              THEN 'Undergraduate'
            WHEN stvclas_desc LIKE 'Medical%'
              THEN 'Medical'
            WHEN stvclas_desc LIKE 'Vet. Med.%'
              THEN 'Vet Med'
            ELSE stvclas_desc
       END class_group_desc,
       RANK() OVER (PARTITION BY stvclas_code ORDER BY stvclas_code, upd_timestamp desc) AS class_rank
FROM    psa_stvclas
WHERE   dls_class = 'Y'
AND     stvclas_desc IS NOT NULL;

------------------------------------------------------------------------------------------------------------------------

DROP TABLE source_stvethn; 

CREATE TABLE source_stvethn
  AS SELECT * FROM stvethn@banner WHERE 1=2;

DROP TABLE psa_stvethn; 

CREATE TABLE psa_stvethn(
  stvethn_code      VARCHAR2(2 CHAR),
  stvethn_desc      VARCHAR2(30 CHAR),
  dls_ethn_student  VARCHAR2(1 CHAR),
  upd_timestamp     TIMESTAMP,
  action            VARCHAR2(10 CHAR)
);

CREATE OR REPLACE FORCE VIEW trnsfrm_ethnicity_student_v
( 
  ethnicity_code,
  ethnicity_desc,
  ethnicity_group_desc,
  ethn_rank
)
AS
SELECT stvethn_code,
       stvethn_desc,
       DECODE(stvethn_code, 'AF', 'African American', 
                            'CH', 'Asian-PI', 
                            'EI', 'Asian-PI', 
                            'FP', 'Asian-PI', 
                            'HW', 'Asian-PI', 
                            'JA', 'Asian-PI', 
                            'KO', 'Asian-PI', 
                            'OA', 'Asian-PI', 
                            'PI', 'Asian-PI', 
                            'SA', 'Asian-PI', 
                            'VT', 'Asian-PI', 
                            'HO', 'Chicano-Latino', 
                            'LA', 'Chicano-Latino', 
                            'MX', 'Chicano-Latino', 
                            'PC', 'Chicano-Latino', 
                            'PM', 'Chicano-Latino', 
                            'AI', 'Native American', 
                            'OT', 'Other/Unknown', 
                            'DS', 'Other/Unknown', 
                            'NO', 'Other/Unknown', 
                            'WH', 'White', 
                            'Other/Unknown') AS ethnicity_group_desc,
        RANK() OVER (PARTITION BY stvethn_code ORDER BY stvethn_code, upd_timestamp desc) AS ethn_rank
FROM    psa_stvethn
WHERE   dls_ethn_student = 'Y'
AND     stvethn_desc IS NOT NULL;

------------------------------------------------------------------------------------------------------------------------

DROP TABLE source_first_gen; 

CREATE TABLE source_first_gen
(
  first_gen_code VARCHAR2(1),
  first_gen_short_desc  VARCHAR2(10)
);

DROP TABLE psa_first_gen; 

CREATE TABLE psa_first_gen
(
  first_gen_code        VARCHAR2(1),
  first_gen_short_desc  VARCHAR2(3),
  dls_first_gen         VARCHAR2(1 CHAR),
  upd_timestamp         TIMESTAMP,
  action                VARCHAR2(10)
);

CREATE OR REPLACE FORCE VIEW trnsfrm_first_gen_v
( 
  first_gen_code,
  first_gen_short_desc,
  first_gen_rank
)
AS
SELECT first_gen_code, 
       first_gen_short_desc,
       RANK() OVER (PARTITION BY first_gen_code ORDER BY first_gen_code, upd_timestamp desc) AS first_gen_rank
FROM   psa_first_gen
WHERE  dls_first_gen = 'Y'
AND    first_gen_code IS NOT NULL;

------------------------------------------------------------------------------------------------------------------------

DROP TABLE source_gender; 

CREATE TABLE source_gender
(
  gender_code    VARCHAR2(1 CHAR),
  gender_desc    VARCHAR2(20 CHAR),
  gender_desc_2  VARCHAR2(20 CHAR)
);

DROP TABLE psa_gender; 

CREATE TABLE psa_gender
(
  gender_code    VARCHAR2(1 CHAR),
  gender_desc    VARCHAR2(20 CHAR),
  gender_desc_2  VARCHAR2(20 CHAR),
  dls_gender     VARCHAR2(1 CHAR),
  upd_timestamp  TIMESTAMP,
  action         VARCHAR2(10)
);

CREATE OR REPLACE FORCE VIEW trnsfrm_gender_v
( 
  gender_code,
  gender_desc,
  gender_desc_2,
  gender_rank
)
AS
SELECT gender_code,
       gender_desc,
       gender_desc_2,
       RANK() OVER (PARTITION BY gender_code ORDER BY gender_code, upd_timestamp desc) AS gender_rank
FROM   psa_gender
WHERE  dls_gender = 'Y'
AND    gender_desc IS NOT NULL
AND    gender_desc_2 IS NOT NULL;

------------------------------------------------------------------------------------------------------------------------

DROP TABLE source_srvc_intl_dom; 

CREATE TABLE source_srvc_intl_dom
(
  intl_dom_code      VARCHAR2(10),
  intl_dom_desc      VARCHAR2(30 CHAR),
  intl_dom_category  VARCHAR2(30 CHAR)
);

DROP TABLE psa_srvc_intl_dom; 

CREATE TABLE psa_srvc_intl_dom
(
  intl_dom_code      VARCHAR2(10),
  intl_dom_desc      VARCHAR2(30 CHAR),
  intl_dom_category  VARCHAR2(30 CHAR),
  dls_intl_dom       VARCHAR2(1 CHAR),
  upd_timestamp      TIMESTAMP,
  action             VARCHAR2(10)
);

CREATE OR REPLACE FORCE VIEW trnsfrm_srvc_intl_dom_v
( 
  intl_dom_code,
  intl_dom_desc,
  intl_dom_category,
  intl_dom_rank
)
AS
SELECT intl_dom_code,
       intl_dom_desc,
       intl_dom_category,
       RANK() OVER (PARTITION BY intl_dom_code ORDER BY intl_dom_code, upd_timestamp desc) AS intl_dom_rank
FROM   psa_srvc_intl_dom
WHERE  dls_intl_dom = 'Y'
AND    intl_dom_desc IS NOT NULL;

------------------------------------------------------------------------------------------------------------------------

DROP TABLE source_stvlevl; 

CREATE TABLE source_stvlevl
  AS SELECT * FROM stvlevl@banner WHERE 1=2;

DROP TABLE psa_stvlevl; 

CREATE TABLE psa_stvlevl
(
  stvlevl_code    VARCHAR2(2 CHAR),
  stvlevl_desc    VARCHAR2(30 CHAR),
  dls_level       VARCHAR2(1 CHAR),
  upd_timestamp   TIMESTAMP,
  action          VARCHAR2(10 CHAR)
);

CREATE OR REPLACE FORCE VIEW trnsfrm_stud_level_v
( 
  level_code,
  level_desc,
  level_rank
)
AS
SELECT stvlevl_code,
       stvlevl_desc,
       RANK() OVER (PARTITION BY stvlevl_code ORDER BY stvlevl_code, upd_timestamp desc) AS level_rank
FROM    psa_stvlevl
WHERE   dls_level = 'Y'
AND     stvlevl_desc IS NOT NULL;

------------------------------------------------------------------------------------------------------------------------

DROP TABLE source_stvterm; 

CREATE TABLE source_stvterm
  AS SELECT * FROM stvterm@banner WHERE 1=2;
  
DROP TABLE psa_stvterm; 

CREATE TABLE psa_stvterm
(
  stvterm_code                 VARCHAR2(6),
  stvterm_desc                 VARCHAR2(30),
  stvterm_start_date           DATE,
  stvterm_end_date             DATE,
  stvterm_fa_proc_yr           VARCHAR2(4),
  stvterm_activity_date        DATE,
  stvterm_fa_term              VARCHAR2(1),
  stvterm_fa_period            NUMBER(2),
  stvterm_fa_end_period        NUMBER(2),
  stvterm_acyr_code            VARCHAR2(4),
  stvterm_housing_start_date   DATE,
  stvterm_housing_end_date     DATE,
  stvterm_system_req_ind       VARCHAR2(1),
  stvterm_cont_prog_ind        VARCHAR2(1),
  stvterm_full_time_ind        VARCHAR2(1),
  stvterm_restrict_hrs_ind     VARCHAR2(1),
  stvterm_trmt_code            VARCHAR2(1),
  stvterm_fa_summer_ind        VARCHAR2(1),
  dls_term                     VARCHAR2(1 CHAR),
  upd_timestamp                TIMESTAMP,
  action                       VARCHAR2(10 CHAR)
);

CREATE OR REPLACE FORCE VIEW trnsfrm_stud_term_v
( 
  banner_term_code,
  term_desc,
  term_cal_yr_desc,
  term_cal_yr_short_desc,
  semester_quarter,
  season,
  first_day_of_instruction,
  last_day_of_instruction,
  census_date,
  fiscal_year,
  cal_yr_ending,
  acad_year,
  acad_year_ending,
  acad_year_long,
  aid_yr,
  aid_yr_ending,
  aid_yr_long,
  term_rank
)
AS
SELECT stvterm_code banner_term_code,
       CASE WHEN REGEXP_LIKE(stvterm_desc, '\d{4}$')
            THEN
              SUBSTR(stvterm_desc, 1, LENGTH(stvterm_desc)-5)
            ELSE
               stvterm_desc
       END term_desc,
       stvterm_desc term_cal_yr_desc,
       CASE SUBSTR(stvterm_code, 5, 2)
              WHEN '01' 
                THEN 'Winter Q-' 
              WHEN '02' 
                THEN 'Spring S-'
              WHEN '03' 
                THEN 'Spring Q-'
              WHEN '04' 
                THEN 'Extra -'
              WHEN '05' 
                THEN 'Summer S1-'
              WHEN '06' 
                THEN 'Summer SS-'
              WHEN '07' 
                THEN 'Summer S2-'
              WHEN '08' 
                THEN 'Summer Q-'
              WHEN '09' 
                THEN 'Fall S-'
              WHEN '10' 
                THEN 'Fall Q-'
              ELSE 
                stvterm_desc || '-' 
       END || SUBSTR(stvterm_code,3,2) Term_Cal_Yr_Short_Desc,
       CASE WHEN REGEXP_LIKE(stvterm_desc, 'Semester')
            THEN
              'Semester'
            WHEN REGEXP_LIKE(stvterm_desc, 'Quarter')
            THEN
              'Quarter'
            ELSE
              'Other'
       END semester_quarter,
       CASE WHEN stvterm_desc LIKE '%Spring%'
            THEN
              'Spring'
            WHEN stvterm_desc LIKE '%Fall%'
            THEN
              'Fall'
            WHEN stvterm_desc LIKE '%Summer%' 
            THEN
              'Summer'
            WHEN stvterm_desc LIKE '%Extra%'
            THEN
              'Summer'
            WHEN stvterm_desc LIKE '%Winter%'
            THEN
              'Winter'
            ELSE
              'Other'
       END season,
       stvterm_start_date first_day_of_instruction,
       stvterm_end_date last_day_of_instruction,
       stvterm_start_date + 20 census_date,
       CASE WHEN EXTRACT(MONTH FROM stvterm_end_date) > 6
            THEN 
              EXTRACT(YEAR FROM stvterm_end_date) || '-'
                  || SUBSTR(EXTRACT(YEAR FROM stvterm_end_date) + 1, 3, 2)
            ELSE
              EXTRACT(YEAR FROM stvterm_end_date) - 1 || '-'
                  || SUBSTR(EXTRACT(YEAR FROM stvterm_end_date), 3, 2)
       END fiscal_year,
       EXTRACT(YEAR FROM stvterm_end_date) cal_yr_ending,
       CASE WHEN stvterm_acyr_code = '9900' /* crosses centuries */
            THEN
              SUBSTR(EXTRACT(YEAR FROM stvterm_end_date)-1, 1, 2) ||
                 SUBSTR(stvterm_acyr_code, 1, 2) || '-' ||
                 SUBSTR(stvterm_acyr_code, 3, 2)
            ELSE
              SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                 SUBSTR(stvterm_acyr_code, 1, 2) || '-' ||
                 SUBSTR(stvterm_acyr_code, 3, 2)
       END acad_yr,
       CASE WHEN stvterm_acyr_code = '9900' /* crosses centuries */
            THEN
              SUBSTR(extract(Year from stvterm_end_date)+1, 1, 2) ||
                 SUBSTR(stvterm_acyr_code, 3, 2)
            ELSE
              SUBSTR(extract(Year from stvterm_end_date), 1, 2) ||
                 SUBSTR(stvterm_acyr_code, 3, 2)
       END acad_yr_ending,
       CASE WHEN stvterm_acyr_code = '9900' /* crosses centuries */
            THEN
              SUBSTR(extract(Year from stvterm_end_date)-1, 1, 2) ||
                 SUBSTR(stvterm_acyr_code, 1, 2) || '-' ||
                 SUBSTR(extract(Year from stvterm_end_date)+1, 1, 2) ||
                 SUBSTR(stvterm_acyr_code, 3, 2)
            ELSE
              SUBSTR(extract(Year from stvterm_end_date), 1, 2) ||
                 SUBSTR(stvterm_acyr_code, 1, 2) || '-' ||
                 SUBSTR(extract(Year from stvterm_end_date), 1, 2) ||
                 SUBSTR(stvterm_acyr_code, 3, 2)
       END acad_yr_long,
       CASE WHEN stvterm_fa_proc_yr = '9900' /* crosses centuries */
            THEN
              SUBSTR(EXTRACT(YEAR FROM stvterm_end_date), 1, 2) ||
                 SUBSTR(stvterm_fa_proc_yr, 1, 2) || '-' ||
                 SUBSTR(stvterm_fa_proc_yr, 3, 2)
            WHEN stvterm_fa_proc_yr IS NULL
            THEN
              CASE WHEN SUBSTR(stvterm_code, 5, 2) IN ('01', '02', '03', '04')
                   THEN
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                       SUBSTR(stvterm_acyr_code, 1, 2) || '-' ||
                       SUBSTR(stvterm_acyr_code, 3, 2)
                   WHEN SUBSTR(stvterm_code, 5, 2) IN ('05', '06', '07')
                   THEN
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                     SUBSTR(stvterm_acyr_code, 3, 2) || '-' ||
                       CAST(CAST(SUBSTR(stvterm_acyr_code, 3, 2) AS NUMBER) + 1 AS CHAR(2))
                   ELSE
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                     SUBSTR(stvterm_acyr_code, 1, 2) || '-' ||
                       CAST(CAST(SUBSTR(stvterm_acyr_code, 1, 2) AS NUMBER) + 1 AS CHAR(2))
              END
            ELSE
              SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                 SUBSTR(stvterm_fa_proc_yr, 1, 2) || '-' ||
                 SUBSTR(stvterm_fa_proc_yr, 3, 2)
       END aid_yr,
       CASE WHEN stvterm_fa_proc_yr = '9900' /* crosses centuries */
            THEN
              SUBSTR(extract(Year from stvterm_end_date)+1, 1, 2) ||
                 SUBSTR(stvterm_fa_proc_yr, 3, 2)
            WHEN stvterm_fa_proc_yr IS NULL
            THEN
              CASE WHEN SUBSTR(stvterm_code, 5, 2) IN ('01', '02', '03', '04')
                   THEN
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                     SUBSTR(stvterm_acyr_code, 3, 2)
                   WHEN SUBSTR(stvterm_code, 5, 2) IN ('05', '06', '07')
                   THEN
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                     CAST(CAST(SUBSTR(stvterm_acyr_code, 3, 2) AS NUMBER) + 1 AS CHAR(2))
                   ELSE
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                     CAST(CAST(SUBSTR(stvterm_acyr_code, 1, 2) AS NUMBER) + 1 AS CHAR(2))
              END
            ELSE
              SUBSTR(extract(Year from stvterm_end_date), 1, 2) ||
                 SUBSTR(stvterm_fa_proc_yr, 3, 2)
       END aid_yr_ending,
       CASE WHEN stvterm_fa_proc_yr = '9900' /* crosses centuries */
            THEN
              SUBSTR(EXTRACT(YEAR FROM stvterm_end_date), 1, 2) ||
                 SUBSTR(stvterm_fa_proc_yr, 1, 2) || '-' ||
                 SUBSTR(extract(Year from stvterm_end_date)+1, 1, 2) ||
                 SUBSTR(stvterm_fa_proc_yr, 3, 2)
            WHEN stvterm_fa_proc_yr IS NULL
            THEN
              CASE WHEN SUBSTR(stvterm_code, 5, 2) IN ('01', '02', '03', '04')
                   THEN
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                       SUBSTR(stvterm_acyr_code, 1, 2) || '-' ||
                       SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                       SUBSTR(stvterm_acyr_code, 3, 2)
                   WHEN SUBSTR(stvterm_code, 5, 2) IN ('05', '06', '07')
                   THEN
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                     SUBSTR(stvterm_acyr_code, 3, 2) || '-' ||
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                       CAST(CAST(SUBSTR(stvterm_acyr_code, 3, 2) AS NUMBER) + 1 AS CHAR(2))
                   ELSE
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                     SUBSTR(stvterm_acyr_code, 1, 2) || '-' ||
                     SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                       CAST(CAST(SUBSTR(stvterm_acyr_code, 1, 2) AS NUMBER) + 1 AS CHAR(2))
              END
            ELSE
              SUBSTR(EXTRACT(YEAR from stvterm_end_date), 1, 2) ||
                 SUBSTR(stvterm_fa_proc_yr, 1, 2) || '-' ||
                 SUBSTR(extract(Year from stvterm_end_date), 1, 2) ||
                 SUBSTR(stvterm_fa_proc_yr, 3, 2)
       END aid_yr_long,
       RANK() OVER (PARTITION BY stvterm_code ORDER BY stvterm_code, upd_timestamp desc) AS term_rank
from   psa_stvterm
WHERE   dls_term  = 'Y'
AND     stvterm_desc IS NOT NULL;  

------------------------------------------------------------------------------------------------------------------------


DROP TABLE source_student_enrollment; 

CREATE TABLE source_student_enrollment
  (
    sterm                 VARCHAR2(6 CHAR),
    aterm                 VARCHAR2(6 CHAR),
    zterm                 VARCHAR2(6 CHAR),
    pidm                  NUMBER(8,0),
    sid                   VARCHAR2(9 CHAR),
    report_type           VARCHAR2(8 CHAR),
    age                   NUMBER,
    sex                   VARCHAR2(1 CHAR),
    ethn                  VARCHAR2(2 CHAR),
    ethn_desc             VARCHAR2(16 CHAR),
    fgen                  VARCHAR2(1 CHAR),
    intl_dom              VARCHAR2(4 CHAR),
    citz                  VARCHAR2(2 CHAR),
    resd                  VARCHAR2(1 CHAR),
    styp                  VARCHAR2(1 CHAR),
    majr                  VARCHAR2(4 CHAR),
    coll                  VARCHAR2(2 CHAR),
    clas                  VARCHAR2(2 CHAR),
    levl                  VARCHAR2(2 CHAR),
    units                 NUMBER(3,1),
    stvterm_acyr_code     VARCHAR2(4 CHAR)
  );


DROP TABLE psa_student_enrollment; 

CREATE TABLE psa_student_enrollment
( 
  sterm           VARCHAR2(6 CHAR),
  pidm            NUMBER(8,0),
  report_type     VARCHAR2(8 CHAR),
  age             NUMBER,
  sex             VARCHAR2(1 CHAR),
  ethn            VARCHAR2(2 CHAR),
  fgen            VARCHAR2(1 CHAR),
  intl_dom        VARCHAR2(4 CHAR),
  citz            VARCHAR2(2 CHAR),
  resd            VARCHAR2(1 CHAR),
  majr            VARCHAR2(4 CHAR),
  clas            VARCHAR2(2 CHAR),
  levl            VARCHAR2(2 CHAR),
  units           NUMBER(3,1),
  fls_stud_enrol  VARCHAR2(1 CHAR),
  upd_timestamp   TIMESTAMP,
  action          VARCHAR2(10)
);

CREATE OR REPLACE FORCE VIEW trnsfrm_student_enrollment_v
( 
  epidm,
  head_count,
  units_att_term,
  term_code,
  admit_type,
  age,
  class_code,
  citizenship,
  major_code,
  first_gen,
  gender,
  eth_grp_code,
  level_code
)
AS
SELECT enc_dec.encrypt(to_char(pidm))epidm,
       1 head_count,
       nvl(units, 0) units,
       sterm,
       nvl(report_type, '-') report_type,
       age,
       clas,
       CASE WHEN intl_dom = 'INTL' THEN 
              'INTL'
            ELSE
              CASE WHEN (intl_dom = 'DOM' AND resd = 'N') THEN 
                     'NAT'
                   ELSE 'CA'
              END
       END citz,
       majr,
       CASE WHEN levl NOT LIKE 'U%' 
            THEN
              'X'
            ELSE
              nvl(fgen, 'U')
       END fgen,
       nvl(sex, 'U') sex,
       nvl(ethn, '-') ethn,
       levl
FROM   psa_student_enrollment
WHERE  fls_stud_enrol = 'Y';

------------------------------------------------------------------------------------------------------------------------